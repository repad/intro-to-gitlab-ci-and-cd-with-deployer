<?php

use LexiSoft\CiCD\MyClass;

require __DIR__ . '/../vendor/autoload.php';

MyClass::shout('Hello world!');

$pdo = new PDO('mysql:dbname=dbname;host=db','root');

var_dump($pdo->errorInfo(), $pdo->errorCode());
